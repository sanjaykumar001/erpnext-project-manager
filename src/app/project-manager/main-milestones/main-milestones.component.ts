import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-milestones',
  templateUrl: './main-milestones.component.html',
  styleUrls: ['./main-milestones.component.css'],
})
export class MainMilestonesComponent implements OnInit {
  milestoneFields = [1, 2, 3, 4, 5, 6, 7];
  panelOpenState = true;
  milestoneFields2 = [1, 2, 3];
  milestones = [
    this.milestoneFields,
    this.milestoneFields2,
    this.milestoneFields,
  ];
  constructor() {}

  ngOnInit() {}
}
